﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace GamesYouService
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da interface "IService1" no arquivo de código e configuração ao mesmo tempo.
    [ServiceContract]
    public interface IService1
    {
        
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/LoginGoogle/userId={userId}&givenName={givenName}&lastName={lastName}&email={email}")]
        User LoginGoogle (string userId, string givenName, string lastName, string email);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "/GuardarUtilizador/id={id}&givenName={givenName}&lastName={lastName}&email={email}&password={password}&steam={steam}&origin={origin}&?image={image}")]
        void GuardarUtilizador(string id, string givenName, string lastName, string email, string password, string steam, string origin, string image);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/Login/userName={userName}&password={password}")]
        User Login(string userName, string password);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RegistarUtilizador/userName={userName}&password={password}&email={email}")]
        User RegistarUtilizador(string userName, string password, string email);

        [OperationContract]
        [WebInvoke(Method = "POST")]
        void GetTopGamesApi();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        Game[] GetTopGames();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/VerEstadoJogo/idUtilizador={idUtilizador}&idJogo={idJogo}")]
        GameUtilizador VerEstadoJogo(string idUtilizador, string idJogo);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/AlterarEstadoJogo/idUtilizador={idUtilizador}&idJogo={idJogo}&meujogo={meujogo}&favorito={favorito}&gameName={gameName}&?cover={cover}")]
        GameUtilizador AlterarEstadoJogo(string idUtilizador, string idJogo, string meujogo, string favorito, string gameName, string cover);
        // TODO: Adicione suas operações de serviço aqui

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetGamesByUser/idUtilizador={idUtilizador}")]
        Game[] GetGamesByUser(string idUtilizador);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/AdicionarComentario/texto={texto}&idjogo={idjogo}&idutilizador={idutilizador}&gameName={gameName}")]
        Comentario AdicionarComentario(string texto, string idjogo, string idutilizador, string gameName);

        [OperationContract]
        [WebInvoke(Method = "DELETE", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/RemoverComentario/id={id}")]
        bool RemoverComentario(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetComentarios/idjogo={idjogo}")]
        Comentario[] GetComentarios(string idjogo);


    }


    // Use um contrato de dados como ilustrado no exemplo abaixo para adicionar tipos compostos a operações de serviço.
    [DataContract]
    public class User
    {
        int id;
        string username;
        string email;
        string firstName;
        string lastName;
        string error;
        string steam;
        string origin;
        string image;

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        [DataMember]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        [DataMember]
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        [DataMember]
        public string Steam
        {
            get { return steam; }
            set { steam = value; }
        }

        [DataMember]
        public string Origin
        {
            get { return origin; }
            set { origin = value; }
        }
        [DataMember]
        public string Image
        {
            get { return error; }
            set { error = value; }
        }
        [DataMember]
        public string Error
        {
            get { return error; }
            set { error = value; }
        }
    }

    [DataContract]
    public class Game
    {
        int _id;
        string _name;
        string _summary;
        double _rating;
        double _popularity;
        double _first_release_date;
        Genre[] _genres;
        Video[] _videos;
        Cover _cover;
        bool _favorito;
        bool _meujogo;
        string _coverUrl;


        [DataMember]
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember]
        public string summary
        {
            get { return _summary; }
            set { _summary = value; }
        }

        [DataMember]
        public double rating
        {
            get { return _rating; }
            set { _rating = value; }
        }

        [DataMember]
        public double popularity
        {
            get { return _popularity; }
            set { _popularity = value; }
        }

        [DataMember]
        public double first_release_date
        {
            get { return _first_release_date; }
            set { _first_release_date = value; }
        }

        [DataMember]
        public Genre[] genres
        {
            get { return _genres; }
            set { _genres = value; }
        }

        [DataMember]
        public Video[] videos
        {
            get { return _videos; }
            set { _videos = value; }
        }

        [DataMember]
        public Cover cover
        {
            get { return _cover; }
            set { _cover = value; }
        }

        [DataMember]
        public bool favorito
        {
            get { return _favorito; }
            set { _favorito = value; }
        }

        [DataMember]
        public bool meujogo
        {
            get { return _meujogo; }
            set { _meujogo = value; }
        }
        [DataMember]
        public string coverUrl
        {
            get { return _coverUrl; }
            set { _coverUrl = value; }
        }
    }

    [DataContract]
    public class GameUtilizador
    {
        int _id;
        int _idUtilizador;
        int _idJogo;
        bool _favorito;
        bool _meujogo;
        string _cover;


        [DataMember]
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public int idUtilizador
        {
            get { return _idUtilizador; }
            set { _idUtilizador = value; }
        }

        [DataMember]
        public int idjogo
        {
            get { return _idJogo; }
            set { _idJogo = value; }
        }

        [DataMember]
        public bool favorito
        {
            get { return _favorito; }
            set { _favorito = value; }
        }

        [DataMember]
        public bool meujogo
        {
            get { return _meujogo; }
            set { _meujogo = value; }
        }

        [DataMember]
        public string cover
        {
            get { return _cover; }
            set { _cover = value; }
        }
    }

    [DataContract]
    public class Video
    {
        string _name;
        string _video_id;

        [DataMember]
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember]
        public string video_id
        {
            get { return _video_id; }
            set { _video_id = value; }
        }
    }

    [DataContract]
    public class Genre
    {
        string _name;
        int _id;

        [DataMember]
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember]
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
    }

    [DataContract]
    public class Cover
    {
        string _url;
        string _cloudinary_id;
        int _width;
        int _height;

        [DataMember]
        public string url
        {
            get { return _url; }
            set { _url = value; }
        }

        [DataMember]
        public string cloudinary_id
        {
            get { return _cloudinary_id; }
            set { _cloudinary_id = value; }
        }


        [DataMember]
        public int width
        {
            get { return _width; }
            set { _width = value; }
        }

        [DataMember]
        public int height
        {
            get { return _height; }
            set { _height = value; }
        }
    }

    [DataContract]
    public class Comentario
    {
        int _id;
        string _texto;
        int _idUtilizador;
        int _idJogo;
        string _email;

        [DataMember]
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        [DataMember]
        public string texto
        {
            get { return _texto; }
            set { _texto = value; }
        }

        [DataMember]
        public int idUtilizador
        {
            get { return _idUtilizador; }
            set { _idUtilizador = value; }
        }

        [DataMember]
        public int idjogo
        {
            get { return _idJogo; }
            set { _idJogo = value; }
        }

        [DataMember]
        public string email
        {
            get { return _email; }
            set { _email = value; }
        }
    }
}
