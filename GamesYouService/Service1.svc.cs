﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using MySql.Data.MySqlClient;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Web.Script.Serialization;

namespace GamesYouService
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da classe "Service1" no arquivo de código, svc e configuração ao mesmo tempo.
    // OBSERVAÇÃO: Para iniciar o cliente de teste do WCF para testar esse serviço, selecione Service1.svc ou Service1.svc.cs no Gerenciador de Soluções e inicie a depuração.
    public class Service1 : IService1
    {
        private MySqlConnection connection;
        private NameValueCollection _AppSettings;
        // static HttpClient client = new HttpClient();

       


        public User LoginGoogle(string userId, string givenName, string lastName, string email)
        {
            try
            {
                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                string decUserId = HttpUtility.HtmlDecode(userId);
                string decGivenName = HttpUtility.HtmlDecode(givenName);
                string decLastName = HttpUtility.HtmlDecode(lastName);
                string decEmail = HttpUtility.HtmlDecode(email);
                User user = new User();
                cmd.CommandText = "SELECT id, userId, userName, firstName, lastName, email, steam, origin, image FROM " + connection.Database + ".Utilizadores where userId = '" + decUserId + "'";

                MySqlDataReader reader = cmd.ExecuteReader();
                if (!reader.HasRows)
                {
                    connection.Close();
                    connection.Open();
                    MySqlCommand cmdInsert = connection.CreateCommand();
                    cmdInsert.CommandText = "INSERT into " + connection.Database + ".Utilizadores (userId, lastName, firstName, email) VALUES (" + decUserId + ", '" + decLastName + "', '" + decGivenName + "', '" + decEmail + "')";
                    cmdInsert.ExecuteNonQuery();
                    connection.Close();
                    connection.Open();
                    cmd.CommandText = "SELECT id, userId, userName, firstName, lastName, email, steam, origin, image FROM " + connection.Database + ".Utilizadores where userId = '" + decUserId + "'";

                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(reader.GetOrdinal("id")))
                            {
                                user.Id = reader.GetInt32("id");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("userName")))
                            {
                                user.Username = reader.GetString("userName");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("email")))
                            {
                                user.Email = reader.GetString("email");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("firstName")))
                            {
                                user.FirstName = reader.GetString("firstName");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("lastName")))
                            {
                                user.LastName = reader.GetString("lastName");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("steam")))
                            {
                                user.Steam = reader.GetString("steam");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("origin")))
                            {
                                user.Origin = reader.GetString("origin");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("image")))
                            {
                                user.Image = reader.GetString("image");
                            }
                        }
                    }
                    else
                    {
                        user.Error = "Utilizador não existia e não foi criado.";
                    }
                }
                else
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("id")))
                        {
                            user.Id = reader.GetInt32("id");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("userName")))
                        {
                            user.Username = reader.GetString("userName");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("email")))
                        {
                            user.Email = reader.GetString("email");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("firstName")))
                        {
                            user.FirstName = reader.GetString("firstName");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("lastName")))
                        {
                            user.LastName = reader.GetString("lastName");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("steam")))
                        {
                            user.Steam = reader.GetString("steam");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("origin")))
                        {
                            user.Origin = reader.GetString("origin");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("image")))
                        {
                            user.Image = reader.GetString("image");
                        }
                    }
                }
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                return user;
            }
            catch (Exception ex)
            {
                User user = new User();
                user.Error = ex.Message;
                connection.Close();
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                return user;
            }
        }

        // POR ACABAR
        public void GuardarUtilizador(string id, string givenName, string lastName, string email, string password, string steam, string origin, string image)
        {
            try
            {
                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                string decId = HttpUtility.HtmlDecode(id);
                string decGivenName = HttpUtility.HtmlDecode(givenName);
                string decLastName = HttpUtility.HtmlDecode(lastName);
                string decEmail = HttpUtility.HtmlDecode(email);
                string decPassword = HttpUtility.HtmlDecode(password);
                string decSteam = HttpUtility.HtmlDecode(steam);
                string decOrigin = HttpUtility.HtmlDecode(origin);
                string decImage = HttpUtility.HtmlDecode(image);
                cmd.CommandText = "UPDATE " + connection.Database + ".Utilizadores SET firstname='" + decGivenName + "', lastname='" + decLastName + "', email='" + decLastName + "', password='" + decPassword + "', steam='" + decSteam + "', origin='" + decOrigin+ "', image='" + decImage + "'  where id = '" + decId + "'";
                cmd.ExecuteNonQuery();
                connection.Close();
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
              
            }
            catch (Exception ex)
            {
                User user = new User();
                user.Error = ex.Message;
                connection.Close();
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
              
            }
        }

        public User Login(string username, string password)
        {
            try
            {
                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                string decUserName = HttpUtility.HtmlDecode(username);
                string decPassword = HttpUtility.HtmlDecode(password);
                User user = new User();
                cmd.CommandText = "SELECT id, userId, userName, firstName, lastName, email, steam, origin, image FROM " + connection.Database + ".Utilizadores where userName = '" + decUserName + "' and password = '" + decPassword + "'";

                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("id")))
                        {
                            user.Id = reader.GetInt32("id");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("userName")))
                        {
                            user.Username = reader.GetString("userName"); 
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("email")))
                        {
                            user.Email = reader.GetString("email"); 
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("firstName")))
                        {
                            user.FirstName = reader.GetString("firstName"); 
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("lastName")))
                        {
                            user.LastName = reader.GetString("lastName"); 
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("steam")))
                        {
                            user.Steam = reader.GetString("steam");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("origin")))
                        {
                            user.Origin = reader.GetString("origin");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("image")))
                        {
                            user.Image = reader.GetString("image");
                        }
                    }
                    connection.Close();
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return user;
                }

                else
                {
                    connection.Close();
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                    user.Error = "User Name ou Password estão incorretos.";
                    return user;
                }
            }
            catch (Exception ex)
            {
                User user = new User();
                user.Error = ex.Message;
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                connection.Close();
                return user;
            }
          
        }

        public User RegistarUtilizador(string userName, string password, string email)
        {
            try
            {
                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                string decUserName = HttpUtility.HtmlDecode(userName);
                string decPassword = HttpUtility.HtmlDecode(password);
                string decEmail = HttpUtility.HtmlDecode(email);


                cmd.CommandText = "SELECT userId FROM " + connection.Database + ".Utilizadores where userName = '" + decUserName + "'";
                User user = new User();
                MySqlDataReader reader = cmd.ExecuteReader();

                if (!reader.HasRows)
                {
                    connection.Close();
                    connection.Open();
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "INSERT INTO Utilizadores (userName, password, email) VALUES ('" + decUserName + "', '" + decPassword + "', '" + decEmail + "')";
                    cmd.ExecuteNonQuery();
                    connection.Close();
                    connection.Open();
                    cmd.CommandText = "SELECT id, userName, lastName, firstName, email FROM " + connection.Database + ".Utilizadores where userName = '" + decUserName + "'";
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    { 
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(reader.GetOrdinal("id")))
                            {
                                user.Id = reader.GetInt32("id");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("userName")))
                            {
                                user.Username = reader.GetString("userName"); 
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("email")))
                            {
                                user.Email = reader.GetString("email"); 
                            }
                        }
                        WebOperationContext ctx = WebOperationContext.Current;
                        ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    }
                    else
                    {
                        user.Error = "Não foi encontrado o utilizador.";
                        WebOperationContext ctx = WebOperationContext.Current;
                        ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                    }
                    connection.Close();

                    return user;
                }
                else
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                    user.Error = "Já existe um utilizador com o User Name definido.";
                    connection.Close();
                    return user;
                }
                    
            }
            catch (Exception ex)
            {
                User user = new User();
                user.Error = ex.Message;
                connection.Close();
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                return user;
            }
        }

        private void GetDataBaseConnection()
        {

            this._AppSettings = ConfigurationManager.AppSettings;
            //SqlConnectionStringBuilder conn_string = new SqlConnectionStringBuilder();
            //conn_string.DataSource = _AppSettings["serverNameBD"];

            //conn_string.InitialCatalog = _AppSettings["DatabaseBD"];
            //conn_string.UserID = _AppSettings["userBD"];
            //conn_string.Password = _AppSettings["passwordBD"];

            string server = _AppSettings["serverNameBD"];
            string database = _AppSettings["DatabaseBD"];
            string uid = _AppSettings["userBD"];
            string password = _AppSettings["passwordBD"];
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
            connection.Open();


        }

        public void GetTopGamesApi()
        {
            try
            {
                NumberFormatInfo nfi = new NumberFormatInfo();
                nfi.NumberDecimalSeparator = ".";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api-2445582011268.apicast.io/games/?fields=name,summary,rating,first_release_date,genres.name,videos,cover,popularity&order=popularity:desc&expand=game,genres&filter[category][eq]=0");

                //request.Headers["Accept"] = "application/json";
                request.Headers["user-key"] = "579aaa6f3fa2b5775a4dd697f68b348c";
                request.Accept = "application/json";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string result = reader.ReadToEnd();
                //JsonReader jsonReader = new JsonReader;
                List<Game> games = JsonConvert.DeserializeObject<List<Game>>(result);

                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE " + connection.Database + ".jogos SET top = 0 where top != 0";
                cmd.ExecuteNonQuery();
                connection.Close();
                int i = 1;
                foreach (Game game in games)
                {
                    connection.Open();
                    //string x = game.Videos.GetType().ToString();
                    if (game.genres == null)
                        game.genres = new Genre[0];
                    if (game.videos == null)
                        game.videos = new Video[0];
                    if (game.summary == null)
                        game.summary = "";

                    var jsonCover = new JavaScriptSerializer().Serialize(game.cover);
                    var jsonVideos = new JavaScriptSerializer().Serialize(game.videos);
                    var jsonGenres = new JavaScriptSerializer().Serialize(game.genres);
                    cmd.CommandText = "INSERT INTO " + connection.Database + ".jogos (id, name,summary,rating, popularity, first_release_date, cover, genres, videos, top) VALUES (" + game.id + ", '" + game.name + "','" + game.summary.Replace("\'", "\\'") + "'," + Math.Round(game.rating,2).ToString().Replace(',', '.') + "," + game.popularity.ToString().Replace(',', '.') + "," + game.first_release_date + ",'" + jsonCover + "', '" + jsonGenres + "', '" + jsonVideos + "', " + i + ") ON DUPLICATE KEY UPDATE name = '" + game.name + "', summary = '" + game.summary.Replace("\'", "\\'") + "', rating = " + Math.Round(game.rating,2).ToString().Replace(',', '.') + ", popularity = " + game.popularity.ToString().Replace(',', '.') + ", first_release_date = " + game.first_release_date + ", cover = '" + jsonCover + "', genres = '" + jsonGenres + "', videos = '" + jsonVideos + "', top = " + i + "; ";
                    cmd.ExecuteNonQuery();
                    connection.Close();
                    i++;
                }
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
            }
            catch (Exception ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;

            }
            //JObject json = JObject.Parse(result);
            //WebRequest request = WebRequest.Create("https://api-2445582011268.apicast.io/games/?fields=name,summary,rating,first_release_date,genres.name,videos,cover,popularity&order=popularity:desc&expand=game,genres&filter[category][eq]=0");
            //request.Headers.Add(new HttpRequestHeader())
        }

        public Game[] GetTopGames()
        {
            try
            {
                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "select id, name, summary, rating, popularity, first_release_date, genres, videos, cover, top from jogos where top != 0 order by top limit 10";

                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    Game[] listGames = new Game[10];
                    int i = 0;
                    while (reader.Read())
                    {
                        Game game = new Game();
                        if (!reader.IsDBNull(reader.GetOrdinal("id")))
                        {
                            game.id = reader.GetInt32("id");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("name")))
                        {
                            game.name = reader.GetString("name");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("summary")))
                        {
                            game.summary = reader.GetString("summary");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("rating")))
                        {
                            game.rating = reader.GetDouble("rating");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("popularity")))
                        {
                            game.popularity = reader.GetDouble("popularity");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("first_release_date")))
                        {
                            game.first_release_date = reader.GetDouble("first_release_date");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("genres")))
                        {
                            JArray genreArray = JArray.Parse(reader.GetString("genres"));
                            game.genres = new Genre[genreArray.Count];
                            int x = 0;
                            foreach (JObject obj in genreArray)
                            {
                                Genre genreClass = new Genre();
                                genreClass.id = int.Parse(obj["id"].ToString());
                                genreClass.name = obj["name"].ToString();
                                game.genres[x] = genreClass;
                                x++;
                            }
                            // game.genres = reader.GetString("genres");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("videos")))
                        {
                            JArray videoArray = JArray.Parse(reader.GetString("videos"));
                            game.videos = new Video[videoArray.Count];
                            int x = 0;
                            foreach (JObject obj in videoArray)
                            {
                                Video videoClass = new Video();
                                videoClass.name = obj["name"].ToString();
                                videoClass.video_id = obj["video_id"].ToString();
                                game.videos[x] = videoClass;
                                x++;
                            }
                           // game.videos = reader.GetString("videos");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("cover")))
                        {
                            if (reader.GetString("cover") != "")
                            {
                                //game.cover = reader.GetString("cover");
                                JObject cover = JObject.Parse(reader.GetString("cover"));
                                //game.cover = new Dictionary<int, string>() { { 111, "teste" } };
                                Cover coverClass = new Cover();
                                coverClass.url = cover["url"].ToString();
                                coverClass.cloudinary_id = cover["cloudinary_id"].ToString();
                                coverClass.height = (cover["height"].ToString() != "" ? int.Parse(cover["height"].ToString()) : 50);
                                coverClass.width = (cover["width"].ToString() != "" ? int.Parse(cover["width"].ToString()) : 50);
                                game.cover = coverClass; 
                            }
                        }
                        listGames[i] = game;
                        i++;
                    }
                    connection.Close();
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return listGames;
                }
                else
                {
                    connection.Close();
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                    return new Game[10];
                }

             
            }
            catch (Exception ex)
            {
                List<Game> listGames = new List<Game>();
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                return new Game[10];
            }
        }

        public GameUtilizador VerEstadoJogo(string idUtilizador, string idJogo)
        {
            try
            {
                GameUtilizador gameUser = new GameUtilizador();
                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "select id, id_Jogo, id_Utilizador, favorito, meujogo, cover from utilizadores_jogos where id_Jogo = '" + idJogo + "' and id_Utilizador = '" + idUtilizador + "'";
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("id")))
                        {
                            gameUser.id = reader.GetInt32("id");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("id_Jogo")))
                        {
                            gameUser.idjogo = reader.GetInt32("id_Jogo");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("id_Utilizador")))
                        {
                            gameUser.idUtilizador = reader.GetInt32("id_Utilizador");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("favorito")))
                        {
                            gameUser.favorito = reader.GetBoolean("favorito");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("meujogo")))
                        {
                            gameUser.meujogo = reader.GetBoolean("meujogo");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("cover")))
                        {
                            gameUser.cover = reader.GetString("cover");
                        }
                    }
                    connection.Close();
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return gameUser;
                }

                return gameUser;
            }
            catch (Exception ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                return new GameUtilizador();
            }
        }

        public GameUtilizador AlterarEstadoJogo(string idUtilizador, string idJogo, string meujogo, string favorito, string gameName, string coverurl)
        {
            try
            {
                GameUtilizador gameUser = new GameUtilizador();
                GetDataBaseConnection();
                string decIdUtilizador = HttpUtility.HtmlDecode(idUtilizador);
                string decIdJogo = HttpUtility.HtmlDecode(idJogo);
                string decName = HttpUtility.HtmlDecode(gameName);
                string decCoverUrl = HttpUtility.HtmlDecode(coverurl);
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "select id, id_Jogo, id_Utilizador, favorito, meujogo, cover from utilizadores_jogos where id_Jogo = '" + decIdJogo + "' and id_Utilizador = '" + decIdUtilizador + "'";
                MySqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    connection.Close();
                    connection.Open();
                    MySqlCommand cmdUpdate = connection.CreateCommand();
                    cmdUpdate.CommandText = "UPDATE " + connection.Database + ".utilizadores_jogos SET favorito='" + favorito + "', meujogo = '" + meujogo + "', cover = '" + decCoverUrl + "' WHERE id_Jogo = '" + decIdJogo + "' and id_Utilizador = '" + decIdUtilizador + "'";
                    cmdUpdate.ExecuteNonQuery();
                    connection.Close();
                    connection.Open();
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "select id, id_Jogo, id_Utilizador, favorito, meujogo, cover from utilizadores_jogos where id_Jogo = '" + decIdJogo + "' and id_Utilizador = '" + decIdUtilizador + "'";
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("id_Jogo")))
                        {
                            gameUser.idjogo = reader.GetInt32("id_Jogo");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("id_Utilizador")))
                        {
                            gameUser.idUtilizador = reader.GetInt32("id_Utilizador");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("favorito")))
                        {
                            gameUser.favorito = reader.GetBoolean("favorito");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("meujogo")))
                        {
                            gameUser.meujogo = reader.GetBoolean("meujogo");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("cover")))
                        {
                            gameUser.cover = reader.GetString("cover");
                        }
                    }
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return gameUser;
                }
                else
                {
                    connection.Close();
                    connection.Open();
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "select id from jogos where id = '" + decIdJogo + "'";
                    reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        connection.Close();
                        connection.Open();
                        MySqlCommand cmdInsertJogo = connection.CreateCommand();
                        cmdInsertJogo.CommandText = "INSERT into " + connection.Database + ".jogos (id, name) VALUES (" + decIdJogo + ", '" + decName + "')";
                        cmdInsertJogo.ExecuteNonQuery();
                    }
                    

                    connection.Close();
                    connection.Open();
                    MySqlCommand cmdInsert = connection.CreateCommand();
                    cmdInsert.CommandText = "INSERT into " + connection.Database + ".utilizadores_jogos (id_Jogo, id_Utilizador, favorito, meujogo, cover) VALUES (" + decIdJogo + ", '" + decIdUtilizador + "', '" + favorito + "', '" + meujogo + "', '" + decCoverUrl + "')";
                    cmdInsert.ExecuteNonQuery();

                    connection.Close();
                    connection.Open();
                    cmd = connection.CreateCommand();
                    cmd.CommandText = "select id, id_Jogo, id_Utilizador, favorito, meujogo, cover from utilizadores_jogos where id_Jogo = '" + decIdJogo + "' and id_Utilizador = '" + decIdUtilizador + "'";
                    reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(reader.GetOrdinal("id_Jogo")))
                            {
                                gameUser.idjogo = reader.GetInt32("id_Jogo");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("id_Utilizador")))
                            {
                                gameUser.idUtilizador = reader.GetInt32("id_Utilizador");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("favorito")))
                            {
                                gameUser.favorito = reader.GetBoolean("favorito");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("meujogo")))
                            {
                                gameUser.meujogo = reader.GetBoolean("meujogo");
                            }
                            if (!reader.IsDBNull(reader.GetOrdinal("cover")))
                            {
                                gameUser.cover = reader.GetString("cover");
                            }

                        }
                        WebOperationContext ctx = WebOperationContext.Current;
                        ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                        return gameUser;
                    }
                    else
                    {
                        WebOperationContext ctx = WebOperationContext.Current;
                        ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                        return new GameUtilizador();
                    }
                }
            }
            catch (Exception ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                throw ex;
            }
        }

        public Game[] GetGamesByUser(string idUtilizador)
        {
            try
            {
             
                GetDataBaseConnection();
                string decIdUtilizador = HttpUtility.HtmlDecode(idUtilizador);
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "select id_Jogo as id, name, favorito, meujogo, utilizadores_jogos.cover  from utilizadores_jogos join jogos on utilizadores_jogos.id_Jogo=jogos.id where utilizadores_jogos.id_Utilizador = " + idUtilizador + " and (utilizadores_jogos.favorito = 1 or utilizadores_jogos.meujogo = 1)";
                MySqlDataReader reader = cmd.ExecuteReader();
                WebOperationContext ctx = WebOperationContext.Current;
                List<Game> listAux = new List<Game>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Game game = new Game();
                        if (!reader.IsDBNull(reader.GetOrdinal("id")))
                        {
                            game.id = reader.GetInt32("id");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("name")))
                        {
                            game.name = reader.GetString("name");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("favorito")))
                        {
                            game.favorito = reader.GetBoolean("favorito");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("meujogo")))
                        {
                            game.meujogo = reader.GetBoolean("meujogo");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("cover")))
                        {
                            game.coverUrl = reader.GetString("cover");
                        }
                        listAux.Add(game);
                    }
                    Game[] arrayGames = new Game[listAux.Count];
                    for (int i = 0; i < listAux.Count; i++)
                    {
                        arrayGames[i] = listAux.ElementAt(i);
                    }
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return arrayGames;
                }
                else
                {
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return new Game[0];
                }
               
            }
            catch (Exception ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                return new Game[0];
            }
        }

        public Comentario AdicionarComentario(string texto, string idjogo, string idutilizador, string gameName)
        {
            try
            {
                Comentario comentario = new Comentario();
                GetDataBaseConnection();
                string decTexto = HttpUtility.HtmlDecode(texto);
                string decIdJogo = HttpUtility.HtmlDecode(idjogo);
                string decName = HttpUtility.HtmlDecode(gameName);
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "select id from jogos where id = '" + decIdJogo + "'";
                MySqlDataReader reader = cmd.ExecuteReader();
                if (!reader.HasRows)
                {
                    connection.Close();
                    connection.Open();
                    MySqlCommand cmdInsertJogo = connection.CreateCommand();
                    cmdInsertJogo.CommandText = "INSERT into " + connection.Database + ".jogos (id, name) VALUES (" + decIdJogo + ", '" + decName + "')";
                    cmdInsertJogo.ExecuteNonQuery();
                }
                connection.Close();
                connection.Open();
                MySqlCommand cmdInsert = connection.CreateCommand();
                cmdInsert.CommandText = "INSERT into " + connection.Database + ".comentarios (id_Jogo, id_Utilizador, texto) VALUES (" + idjogo + ", " + idutilizador + ", '" + decTexto + "')";
                cmdInsert.ExecuteNonQuery();
                connection.Close();
                connection.Open();
                comentario.texto = decTexto;
                cmd = connection.CreateCommand();
                cmd.CommandText = "select id, id_Jogo, id_utilizador from " + connection.Database + ".comentarios where id_Jogo= " + idjogo + " and id_utilizador = " + idutilizador + " and texto = '" + decTexto + "' order by id desc limit 1";
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("id")))
                        {
                            comentario.id = reader.GetInt32("id");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("id_Jogo")))
                        {
                            comentario.idjogo = reader.GetInt32("id_Jogo");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("id_utilizador")))
                        {
                            comentario.idUtilizador = reader.GetInt32("id_utilizador");
                        }
                    }                    
                }
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                return comentario;
            }
            catch (Exception ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                throw ex;
            }
        }

        public bool RemoverComentario(string id)
        {
            try
            {
                GetDataBaseConnection();
             
                MySqlCommand cmddelete = connection.CreateCommand();
                cmddelete.CommandText = "delete from " + connection.Database + ".comentarios where id = " + id +"";
                cmddelete.ExecuteNonQuery();
                connection.Close();
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                return true;
            }
            catch (Exception ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                return false ;
            }
        }

        public Comentario[] GetComentarios(string idjogo)
        {
            try
            {
                GetDataBaseConnection();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "select c.id, id_Jogo, id_utilizador, texto, email from " + connection.Database + ".comentarios as c join " + connection.Database + ".utilizadores as u on c.id_utilizador=u.id  where id_Jogo= " + idjogo;
                MySqlDataReader reader = cmd.ExecuteReader();
                List<Comentario> listAux = new List<Comentario>();
                WebOperationContext ctx = WebOperationContext.Current;
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Comentario com = new Comentario();
                        if (!reader.IsDBNull(reader.GetOrdinal("id")))
                        {
                            com.id = reader.GetInt32("id");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("id_Jogo")))
                        {
                            com.idjogo = reader.GetInt32("id_Jogo");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("id_utilizador")))
                        {
                            com.idUtilizador = reader.GetInt32("id_utilizador");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("texto")))
                        {
                            com.texto = reader.GetString("texto");
                        }
                        if (!reader.IsDBNull(reader.GetOrdinal("email")))
                        {
                            com.email = reader.GetString("email");
                        }
                        listAux.Add(com);
                    }
                    Comentario[] arrayComentarios = new Comentario[listAux.Count];
                    for (int i = 0; i < listAux.Count; i++)
                    {
                        arrayComentarios[i] = listAux.ElementAt(i);
                    }
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return arrayComentarios;
                }
                else
                {
                    ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)200;
                    return new Comentario[0];
                }
            }
            catch (Exception ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = (System.Net.HttpStatusCode)420;
                return new Comentario[0];
            }
        }

    }
}
